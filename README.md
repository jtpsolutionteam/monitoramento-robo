# Monitoramento dos robôs
Aplicação web feita para monitoramento do status dos Robôs: Matran, Microled e Dta.

## Tecnologias
- React
- Typescript
- Axios

## Como instalar?
```bash
# Clone o repositório
$ git clone https://bitbucket.org/jtpsolutionteam/monitoramento-robo

# Navegue até ele
$ cd monitoramento-robo

# Instale tudo
$ yarn

# Rode em modo de desenvolvimento
$ yarn start

# Faça o build do projeto
$ yarn build
```