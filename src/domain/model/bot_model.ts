type constructorParams = {
  name: string,
  status: BotStatus,
  update: Date,
  businessHourStart: Date | null,
  businessHourEnd: Date | null,
  businessHourDescription: String;
  workAllTime: boolean;
}

type verifyHourInRangeParams = {
  hourStart: Date,
  hourEnd: Date,
  currentHour: Date,
}

type setBotStatusParams = {
  status: BotStatus;
  workAllTime: boolean;
  businessHourStart: Date;
  businessHourEnd: Date;
  currentHour: Date;
}

export enum BotStatus {
  online = "Online",
  offline = "Offline",
  disable = "Desabilitado"
}

export class BotModel {
  name: string;
  status: BotStatus;
  update: Date;
  businessHourStart: Date | null;
  businessHourEnd: Date | null;
  businessHourDescription: String;
  workAllTime: boolean;

  constructor (params: constructorParams) {
    const { name, status, update, businessHourEnd, businessHourStart, businessHourDescription, workAllTime } = params;

    this.name = name;
    this.update = update;
    this.workAllTime = workAllTime;

    this.businessHourEnd = businessHourEnd;
    this.businessHourStart = businessHourStart;
    this.businessHourDescription = businessHourDescription;

    this.status = this._setBotStatus({
      status: status,
      workAllTime: workAllTime,
      currentHour: update,
      businessHourStart: businessHourStart === null ? new Date() : businessHourStart,
      businessHourEnd: businessHourEnd === null ? new Date() : businessHourEnd,
    });
  }

  getInformationBot () {
    return {
      name: this.name,
      status: this.status,
      update: this.update,
    }
  }

  getBusinessHourDescription () {
    return this.businessHourDescription;
  }

  //Verifica a melhor estratégia de verificar o status do Bot
  _setBotStatus (params: setBotStatusParams): BotStatus {
    const { status, businessHourEnd, businessHourStart, currentHour, workAllTime } = params;

    switch (status) {
      case BotStatus.online:
        return status;
      case BotStatus.offline:
        const isMissingParams = (businessHourEnd === null && businessHourStart === null);

        if (workAllTime) {
          return status;
        }

        if (!workAllTime && isMissingParams) {
          throw Error("Horário de funcionamento não encontrado!");
        }

        if (!workAllTime && !isMissingParams) {
          return this._verifyBotStatus({
            currentHour: currentHour,
            hourStart: businessHourStart,
            hourEnd: businessHourEnd,
          });
        }
        return BotStatus.offline;
      default:
        return BotStatus.offline;
    }
  }

  ///Verifica se o robô está offline ou desabilitado
  _verifyBotStatus (params: verifyHourInRangeParams): BotStatus {
    const { currentHour, hourEnd, hourStart } = params;

    const _hourStart = hourStart.getHours();
    const _minutesStart = hourStart.getMinutes();
    const _secondsStart = hourStart.getSeconds();

    const _hourEnd = hourEnd.getHours();
    const _minutesEnd = hourEnd.getMinutes();
    const _secondsEnd = hourEnd.getSeconds();

    const _currentHour = currentHour.getHours();
    const _currentMinutes = currentHour.getMinutes();
    const _currentSeconds = currentHour.getSeconds();

    const START_IN_SECONDS = (_hourStart * 60 * 60) + (_minutesStart * 60) + _secondsStart;
    const END_IN_SECONDS = (_hourEnd * 60 * 60) + (_minutesEnd * 60) + _secondsEnd;
    const CURRENT_IN_SECONDS = (_currentHour * 60 * 60) + (_currentMinutes * 60) + _currentSeconds;

    if (CURRENT_IN_SECONDS < START_IN_SECONDS || CURRENT_IN_SECONDS > END_IN_SECONDS) {
      return BotStatus.disable;
    } else {
      return BotStatus.offline;
    }
  }
}