import { BotModel } from '../model';

export interface GetMonitoringInformation {
  getMonitoringInformation: () => Promise<BotModel[]>
}