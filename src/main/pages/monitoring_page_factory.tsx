import { ApiGetMonitoringInformationFactory } from '..';
import { MonitoringPage } from '../../ui';

export function MonitoringPageFactory () {
  return <MonitoringPage
    getInfo={ ApiGetMonitoringInformationFactory() }
  />;
}