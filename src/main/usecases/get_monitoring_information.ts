import { ApiGetMonitoringInformation } from '../../data/usecases';

export function ApiGetMonitoringInformationFactory() {
  const getInfo = new ApiGetMonitoringInformation();
  return getInfo;
}