import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50rem;
  max-width: 100%;
  margin: 0 auto;
  margin-top: 10%;

  svg {
    width: 10rem;
    margin-right: 2rem;  
  }

  h2 {
    font-size: 2rem;
  }


  @media screen and (max-width: 1024px) {
    h2 {
      font-size: 1.5rem;
    }
    
    svg {
      width: 5rem;
      margin-right: 2rem;
    }
  }
`;
