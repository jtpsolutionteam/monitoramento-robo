import React from 'react';
import { ErrorIcon } from '../../assets';
import { Container } from './styles';


const ErrorFetching: React.FC = () => {
  return (
    <Container>
      <ErrorIcon />
      <h2>Não foi possível buscar os dados</h2>
    </Container>
  );
}

export default ErrorFetching;