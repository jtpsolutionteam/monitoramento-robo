import { ReactComponent as AmalogLogo } from './amalog.svg';
import { ReactComponent as ErrorIcon } from './close.svg';

export {
  AmalogLogo, ErrorIcon
}