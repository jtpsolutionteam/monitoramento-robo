import React, { useEffect, useState } from 'react';
import { BotModel, BotStatus } from '../../domain/model';
import { GetMonitoringInformation } from '../../domain/usecases';
import { AmalogLogo } from '../assets';
import ErrorFetching from '../components/error/error_fetching';
import { Sound } from './sound';
import { Card, CardBot, Content, GridCard, LastUpdate, LogoContainer, Main, Title } from './styles';

type MonitoringPageProps = {
  getInfo: GetMonitoringInformation
}

export function MonitoringPage (props: MonitoringPageProps): JSX.Element {
  const [ listBot, setListBot ] = useState<BotModel[]>([]);
  const [ lastUpdate, setLastUpdate ] = useState(new Date());
  const [ hasError, setHasError ] = useState(false);
  const sound = new Sound();

  useEffect(() => {
    props.getInfo.getMonitoringInformation()
      .then((response) => setListBot(response))
      .then(() => setHasError(false))
      .catch(function (error) {
        console.log('Erro ao buscar dados da API');
        setHasError(true);
      });
  }, [ props.getInfo ]);

  //Loop request
  useEffect(() => {
    const interval = setInterval(function () {
      props.getInfo.getMonitoringInformation()
        .then((response) => setListBot(response))
        .then(() => setHasError(false))
        .catch(function (error) {
          console.log('Erro ao buscar dados da API');
          setHasError(true);
        });
      setLastUpdate(new Date());
    }, 10000);

    return () => clearInterval(interval);
  }, [ props.getInfo ]);

  function getBackgroundColor (status: BotStatus): string {
    switch (status) {
      case BotStatus.online:
        return "#80b918";
      case BotStatus.offline:
        return "#c1121f";
      default:
        return "#4b5563";
    }
  }

  return (
    <Main>
      <LogoContainer>
        <AmalogLogo />
      </LogoContainer>

      <Content>
        <Title>Monitoramento</Title>
        <LastUpdate>Última atualização: { lastUpdate.toLocaleString() }</LastUpdate>

        <GridCard>
          {
            listBot.length > 0 ?
              listBot.map((bot, index) =>
                <Card key={ index }>
                  <CardBot
                    backgroundColor={ getBackgroundColor(bot.status) }
                    textColor={ "#ffffff" }
                  >
                    <div>
                      <h2>{ bot.name }</h2>
                      <p>{ bot.update.toLocaleString() }</p>
                    </div>
                    <footer> { bot.status.toString() } </footer>
                  </CardBot>

                  <footer> { bot.businessHourDescription.toString() } </footer>
                </Card>
              )
              : <></>
          }
        </GridCard>

        { hasError ? <ErrorFetching /> : <></> }

        {
          listBot.forEach((bot) => {
            if (bot.status === BotStatus.offline && bot.name !== 'Microled') {
              sound.play();
            }
          })
        }

      </Content>
    </Main>
  )
}