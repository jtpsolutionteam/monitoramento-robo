import styled, { css } from 'styled-components';

interface CardBotProps {
  backgroundColor: string;
  textColor: string;
}

const sharedShadow = css`
  -webkit-box-shadow: 2px 2px 8px 0px rgba(0, 0, 0, 0.15);
  -moz-box-shadow: 2px 2px 8px 0px rgba(0, 0, 0, 0.15);
  box-shadow: 2px 2px 8px 0px rgba(0, 0, 0, 0.15);
`;

export const Main = styled.main`
  width: 100%;
  height: 100%;  
`;

export const LogoContainer = styled.aside`
  width: 5rem;
  height: 100%;
  min-height: 100vh;
  position: fixed;
  background-color: var(--amalog-color);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  svg {
    max-width: 3.75rem;
  }

  @media screen and (max-width: 1024px) {
    display: none;
  }
`;

export const Content = styled.div`
  max-width: 150rem;
  height: 100%;
  min-height: 100vh;
  padding: 2.4rem 2rem 2.4rem 6rem;

  @media screen and (max-width: 1024px) {
    padding: 2rem 1rem;
  }
`;

export const Title = styled.h1`
  font-weight: 900;
  font-size: 3rem;
  margin-bottom: 0.5rem;

  @media screen and (max-width: 1024px) {
    font-size: 2rem;
  }
`;

export const LastUpdate = styled.p`
  font-size: 1.5rem;

  @media screen and (max-width: 1024px) {
    font-size: 1rem;
  }  
`;

export const GridCard = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  gap: 1.5rem; 
  margin-bottom: 2.5rem;
  margin-top: 2.5rem;

  @media screen and (max-width: 1024px) {
    grid-template-columns: 1fr;
  }
`;

export const Card = styled.div`
  ${sharedShadow};
  background-color: #fff;
  width: 100%;
  height: 22rem;
  border-radius: 1rem;
  z-index: 0;
  
  footer {
    font-size: 1.8rem;
    text-align: center;
    height: 4rem;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  @media screen and (max-width: 1024px) {
    height: 15rem;  
    
    footer {
      font-size: 1.5rem;    
    }
  }
`;


export const CardBot = styled.div<CardBotProps>`
  z-index: 1;
  width: 100%;  
  max-height: 18rem;
  height: 18rem;
  border-radius: 1rem;  
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 2rem 1rem;
  text-align: center;
  background-color: ${(props) => props.backgroundColor};
  color: ${(props) => props.textColor};

  h2 {
    font-size: 2rem;
  }

  p {
    font-size: 1.5rem;
  }

  footer {
    font-size: 2rem;
    font-weight: bold;
  }

  @media screen and (max-width: 1024px) {
    max-height: 11rem;
    height: 11rem;

    h2 {
      font-size: 1.5rem;
    }

    p {
      font-size: 1rem;
    }

    footer {
      font-size: 1.5rem;
      font-weight: bold;
    }
  }
`;

