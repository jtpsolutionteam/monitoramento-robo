export class Sound {
  play (): void {
    const context = new AudioContext();
    const contextGain = context.createGain();

    const oscillator = context.createOscillator();
    oscillator.frequency.value = 493.9;
    oscillator.type = 'sine';

    oscillator.connect(contextGain);
    contextGain.connect(context.destination);
    oscillator.start(0);

    setTimeout(() => {
      contextGain.gain.exponentialRampToValueAtTime(
        0.00001, context.currentTime + 0.04
      );
    }, 1000);
  }
}
