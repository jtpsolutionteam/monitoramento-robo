import axios from 'axios';
import { BotModel, BotStatus } from '../../domain/model';
import { GetMonitoringInformation } from '../../domain/usecases';
import { http_url_api } from '../../infra/http';
import { convertStringToDate } from '../../utils';
import { ApiMonitoringModel } from '../model';

export class ApiGetMonitoringInformation implements GetMonitoringInformation {
  stringToBotStatus (value: string) {
    return value === "LIGADO" ? BotStatus.online : BotStatus.offline;
  }

  async getMonitoringInformation () {
    try {
      const response = await axios.get<ApiMonitoringModel>(http_url_api);

      if (response.status !== 200) {
        const message = `
          Não foi possível buscar os dados\n
          Response Status: ${response.status}
        `;
        console.error(message);
        throw new Error(message);
      }

      const listBots: BotModel[] = [];

      const { dtRoboDta, dtRoboMantran, dtRoboMicroled, txStatusDta, txStatusMantran, txStatusMicroled } = response.data;

      listBots.push(new BotModel({
        name: "Matran",
        status: this.stringToBotStatus(txStatusMantran),
        update: convertStringToDate(dtRoboMantran),
        businessHourDescription: "24h por dia",
        businessHourEnd: null,
        businessHourStart: null,
        workAllTime: true,
      }));

      listBots.push(new BotModel({
        name: "Microled",
        status: this.stringToBotStatus(txStatusMicroled),
        update: convertStringToDate(dtRoboMicroled),
        businessHourDescription: "24h por dia",
        businessHourEnd: null,
        businessHourStart: null,
        workAllTime: true,
      }));

      // DTA - Funcionamento das 06h às 23h
      const DTA_START = new Date(2020, 10, 30, 6, 0, 0);
      const DTA_END = new Date(2020, 10, 30, 23, 0, 0);

      listBots.push(new BotModel({
        name: "Dta",
        status: this.stringToBotStatus(txStatusDta),
        update: convertStringToDate(dtRoboDta),
        businessHourDescription: "Das 06h00 às 23h00",
        businessHourEnd: DTA_END,
        businessHourStart: DTA_START,
        workAllTime: false,
      }));

      return listBots;
    } catch (error) {
      throw Error("Erro ao buscar os dados");
    }
  };

}