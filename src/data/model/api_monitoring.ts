export type ApiMonitoringModel = {
  cdCodigo: number;
  dtRoboMantran: string;
  dtRoboMicroled: string;
  dtRoboDta: string;
  txStatusMantran: string;
  txStatusMicroled: string;
  txStatusDta: string;
}