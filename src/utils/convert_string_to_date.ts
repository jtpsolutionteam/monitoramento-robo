///Exemplo de entrada: 12/03/2021 12:47:20
export function convertStringToDate (value: string): Date {
  try {
    const splitDateTime = value.split(' ');

    const splitDate = splitDateTime[ 0 ];
    const splitTime = splitDateTime[ 1 ];

    const splitDateEspecific = splitDate.split('/');
    const day = Number(splitDateEspecific[ 0 ]);
    const month = Number(splitDateEspecific[ 1 ]);
    const year = Number(splitDateEspecific[ 2 ]);

    const splitTimeEspecific = splitTime.split(':');
    const hour = Number(splitTimeEspecific[ 0 ]);
    const minutes = Number(splitTimeEspecific[ 1 ]);
    const seconds = Number(splitTimeEspecific[ 2 ]);

    return new Date(year, month - 1, day, hour, minutes, seconds);
  } catch (error) {
    console.error(error);
    return new Date();
  }
}
